//
//  main.m
//  EmbedPython
//
//  Created by Kerberos Zhang on 13-3-22.
//  Copyright (c) 2013年 Kerberos Zhang. All rights reserved.
//

#import "Python.h"
#import <UIKit/UIKit.h>

#import "AppDelegate.h"

static void set_python_path (void)
{
    NSString* bundle_path = [[NSString alloc] initWithFormat:@"%@/pylib",[[NSBundle mainBundle] bundlePath]];
    setenv("PYTHONPATH", [bundle_path cStringUsingEncoding:NSUTF8StringEncoding], 1);
    [bundle_path release];
}

int main(int argc, char *argv[])
{
    @autoreleasepool {
        set_python_path();
        Py_SetProgramName(argv[0]);
        Py_Initialize();
        PyRun_SimpleString("import sys\n"
                           "print sys.version\n"
                           "import os\n"
                           "print os.getcwd()\n");
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
