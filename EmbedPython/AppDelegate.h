//
//  AppDelegate.h
//  EmbedPython
//
//  Created by Kerberos Zhang on 13-3-22.
//  Copyright (c) 2013年 Kerberos Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
